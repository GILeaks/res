import json

with open(f'json/TextMap_KR.json', encoding='utf-8') as textmap_json:
    textmap = json.load(textmap_json)

with open(f'json/LoadingTipsExcelConfigData.json', encoding='utf-8') as loadingtips:
    loading = json.load(loadingtips)

loadinglist = []

for i in loading:
    new_obj = {
        "Title": textmap[str(i["tipsTitle"])],
        "Desc": textmap[str(i["tipsDesc"])]
    }
    loadinglist.append(new_obj)


with open('loading_output.json', 'w', encoding='utf-8') as output_file:
    json.dump(loadinglist, output_file, indent=4, ensure_ascii=False)