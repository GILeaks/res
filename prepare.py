import urllib.request, zipfile, subprocess, shutil, platform

xorKey = "0x96"

supportLanguage = {
    "readAble": {
        "KR": "28383130" # todo
    },
    "textMap": {
        "CHS": "26692920",
        "CHT": "27251172",
        "DE": "25181351",
        "EN": "25776943",
        "ES": "20618174",
        "FR": "25555476",
        "ID": "30460104",
        "JP": "32244380",
        "KR": "22299426",
        "PT": "23331191",
        "RU": "21030516",
        "TH": "32056053",
        "VI": "34382464",
        "IT": "27270675",
        "TR": "21419401"
    }
}

# exclude this if not needed
textMapLanguageList = supportLanguage["textMap"].keys()
readAbleLanguageList = supportLanguage["readAble"].keys()

# asset_studio = "?"
mapped_ai_json = "https://github.com/14eyes/gi-asset-indexes/blob/master/mapped/GenshinImpact_3.3.50_beta.zip_31049740.blk.asset_index.json?raw=true"

if __name__ == "__main__":
    print("Res one-click script")
    print("Remember to put blks in Res/blk folder")
    print("")

    shutil.rmtree("./bin/", ignore_errors=True)

    print("Downloading AI json...")
    urllib.request.urlretrieve(mapped_ai_json, "ai.json")


    # ExcelBinOutput
    subprocess.run(["./studio/AssetStudioCLI.exe", "./blk/25539185.blk", "./ExcelBinOutput/", "--game", "GI", "--ai_file", "./ai.json", "--type", "MiHoYoBinData", "--xor_key", xorKey])
    shutil.move("./ExcelBinOutput/MiHoYoBinData/", "./bin/ExcelBinOutput")
    shutil.rmtree("./ExcelBinOutput")

    # Readable
    """
    for readAbleLanguage in readAbleLanguageList:
        try:
            subprocess.run(["./studio/AssetStudioCLI.exe", "./blk/" + supportLanguage["readAble"][readAbleLanguage] + ".blk", "./Readable_" + readAbleLanguage + "/", "--game", "GI", "--ai_file", "./ai.json", "--type", "MiHoYoBinData"])
            shutil.move("./Readable_" + readAbleLanguage + "/MiHoYoBinData/", "./bin/Readable_" + readAbleLanguage)
            shutil.rmtree("./Readable_" + readAbleLanguage)
        except:
            pass
    """

    # TextMap
    for textMapLanguage in textMapLanguageList:
        try:
            subprocess.run(["./studio/AssetStudioCLI.exe", "./blk/" + supportLanguage["textMap"][textMapLanguage] + ".blk", "./TextMap_" + textMapLanguage + "/", "--game", "GI", "--ai_file", "./ai.json", "--type", "MiHoYoBinData", "--xor_key", xorKey])
            shutil.move("./TextMap_" + textMapLanguage + "/MiHoYoBinData/", "./bin/TextMap_" + textMapLanguage)
            shutil.rmtree("./TextMap_" + textMapLanguage)
        except:
            pass

    print("Done")
