import character, weapon, parse, sys, argparse, prepare, questlog
sys.path.append("./py")

from output import Output

# add json if u want
parseList = {
    "AvatarCodexExcelConfigData": Output.AvatarCodexExcelConfig,
    "AvatarCurveExcelConfigData": Output.AvatarCurveExcelConfig,
    "AvatarLevelExcelConfigData": Output.AvatarLevelExcelConfig,
    "AvatarExcelConfigData": Output.AvatarExcelConfig,
    "AvatarSkillDepotExcelConfigData": Output.AvatarSkillDepotExcelConfig,
    "AvatarSkillExcelConfigData": Output.AvatarSkillExcelConfig,
    "FetterInfoExcelConfigData": Output.FetterInfoExcelConfig,
    "FettersExcelConfigData": Output.FettersExcelConfig,
    "FetterStoryExcelConfigData": Output.FetterStoryExcelConfig,
    "AvatarTalentExcelConfigData": Output.AvatarTalentExcelConfig,
    "AvatarPromoteExcelConfigData": Output.AvatarPromoteExcelConfig,
    "ProudSkillExcelConfigData": Output.ProudSkillExcelConfig,
    "MaterialExcelConfigData": Output.MaterialExcelConfig,
    "EquipAffixExcelConfigData": Output.EquipAffixExcelConfig,
    "WeaponExcelConfigData": Output.WeaponExcelConfig,
    "WeaponCurveExcelConfigData": Output.WeaponCurveExcelConfig,
    "WeaponPromoteExcelConfigData": Output.WeaponPromoteExcelConfig,
    "WeaponLevelExcelConfigData": Output.WeaponLevelExcelConfig,
    "ReliquaryExcelConfigData": Output.ReliquaryExcelConfig,
    "ReliquarySetExcelConfigData": Output.ReliquarySetExcelConfig,

    "ChapterExcelConfigData": Output.ChapterExcelConfig,
    "MainQuestExcelConfigData": Output.MainQuestExcelConfig,
    "QuestExcelConfigData": Output.QuestExcelConfig,
    "QuestSummarizationTextExcelConfigData": Output.QuestSummarizationTextExcelConfig,
    "TalkExcelConfigData": Output.TalkExcelConfig,
    "DialogExcelConfigData": Output.DialogExcelConfig,
    "NpcExcelConfigData": Output.NpcExcelConfig,
    "RewardExcelConfigData": Output.RewardExcelConfig,

    "LoadingTipsExcelConfigData": Output.LoadingTipsExcelConfig
}

def printUsage():
    print("""
usage: main.py [-t] [-e] [-c CID] [-o] [-l LANG] [-i ID] [-s] [-w] [-x]

Arguments:
    -t --textmap        # Dump TextMap (-l argument needed)
    -e --excel          # Dump ExcelBinOutput
    -c --chapter [CID]   # Dump ChapterExcelConfigData
    -o --output         # Generate character output (-l, -i argument needed)

    -l --lang [LANG]    # Set language (Example: KR) (ALL available)
    -i --id [ID]        # Set character id (Example: 10000078)

    -x --xlsx           # Xlsx generate
    -s --short          # Xlsx skill short version

    -w --weapon         # Generate weapon output (-l, -i argument needed)
    """)
    sys.exit(1)

if __name__ == "__main__":
    print("Res BLK Parser Tool")
    print("Place .blk files in the Res/blk folder and run prepare.py first\n")

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--textmap", dest="textmap", action="store_true")     # dump textmap
    parser.add_argument("-e", "--excel", dest="excel", action="store_true")         # dump excel
    parser.add_argument("-c", "--chapter", dest="chapter", action="store")         # dump chapter
    parser.add_argument("-o", "--output", dest="output", action="store_true")       # generate output

    parser.add_argument("-l", "--lang", dest="lang", action="store")                # set language
    parser.add_argument("-i", "--id", type=int, dest="id", action="store")                    # set character id

    parser.add_argument("-x", "--xlsx", dest="xlsx", action="store_true")
    parser.add_argument("-s", "--short", dest="short", action="store_true")         # skill short

    parser.add_argument("-w", "--weapon", dest="weapon", action="store_true")
    
    args = parser.parse_args()

    if len(sys.argv) < 2:
        printUsage()

    if args.textmap:
        if args.lang is not None:
            if args.lang == "ALL":
                for i in prepare.supportLanguage["textMap"].keys():
                    parse.GetAllTextmaps(i)
            else:
                parse.GetAllTextmaps(args.lang)
        else:
            printUsage()
    
    if args.excel:
        for i in parseList.keys():
            print("Parsing " + i)
            parse.UniversalParse(i, parseList[i])
    
    if args.output:
        if args.lang is not None and args.id is not None:
            print("Generating res...")
            if args.lang == "ALL":
                for i in prepare.supportLanguage["textMap"].keys():
                    character.GenerateRes(args.id, i, args.xlsx, args.short)
            else:
                character.GenerateRes(args.id, args.lang, args.xlsx, args.short)
        else:
            printUsage()
    
    if args.weapon:
        if args.lang is not None and args.id is not None:
            print("Generating weapon res...")
            if args.lang == "ALL":
                for i in prepare.supportLanguage["textMap"].keys():
                    weapon.GenerateRes(args.id, i, args.xlsx)
            else:
                weapon.GenerateRes(args.id, args.lang, args.xlsx)
        else:
            printUsage()

    if args.chapter:
        if args.chapter == 'ALL':
            chapterId = None
        else:
            chapterId = int(args.chapter)

        if args.lang is not None:
            print("Generating chapter data...")
            if args.lang == "ALL":
                for i in prepare.supportLanguage["textMap"].keys():
                    questlog.GenerateChapter(args.lang, chapterId)
            else:
                questlog.GenerateChapter(args.lang, chapterId)
        else:
            printUsage()
